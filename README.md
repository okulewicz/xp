# Extreme Programming (Programowanie Ekstremalne)
## Podstawy zwinnego wytwarzanie oprogramowania w zgodzie w zasadami Programowania Ekstremalnego (XP)
## Autor: Michał Okulewicz
## Data utworzenia: 21.10.2019
## Liczba dni: 2

#### Grupa docelowa:
Szkolenie przeznaczone jest dla zespołów programistów pragnących poprawić jakość i efektywność swojej pracy.

#### Cel szkolenia:
Celem szkolenia jest zapoznanie uczestników z teoretycznymi oraz praktycznymi
zagadnieniami dotyczącymi zwinnego wytwarzania oprogramowania w oparciu
o zasady programowania ekstremalnego.

Osoby, które ukończą szkolenie dowiedzą się jak:

* Realizować podejście do wytwarzania oprogramowania w oparciu o Test Driven Development
* Dbać o jakość i spójność kodu poprzez programowanie w parach
* Tworzyć środowisko wspierające częste dostawy kolejnych przyrostów

#### Wymagania:

* Umiejętność programowania w jednym z wysokopoziomowych języków obiektowych (Java, C#)
* Ukończone studia informatyczne lub przynajmniej roczne doświadczenie w pracy jako programista

#### Parametry szkolenia:

Wielkość grupy: min. 4 - maks. 8 osób.


### Program szkolenia

1. Manifest zwinnego wytwarzania oprogramowania (Agile Manifesto)
2. Manifest rzemieślników oprogramowania (Sofware Craftsmanship Manifesto)
3. Praktyki Programowania Ekstremalnego
    a.

4. Programowanie Ekstremalne a inne podejścia zwinne